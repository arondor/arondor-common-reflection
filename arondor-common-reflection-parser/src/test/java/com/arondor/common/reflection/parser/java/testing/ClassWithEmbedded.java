package com.arondor.common.reflection.parser.java.testing;

import com.arondor.common.management.mbean.annotation.Description;

@Description("This class is for testing")
public class ClassWithEmbedded
{
    private EmbeddedClass embeddedClass;

    private LANGUAGES languages;

    private ThreadPoolManagerEventHandler threadPoolManager;

    public static interface ThreadPoolManagerEventHandler
    {
        public void onExecutorCreation();
    }

    public class EmbeddedClass
    {
        private String value;

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
    }

    public enum LANGUAGES
    {
        EN(0), FR(1);

        private final int index;

        private LANGUAGES(int value)
        {
            this.index = value;
        }

        public int getIndex()
        {
            return index;
        }
    }

    public EmbeddedClass getEmbeddedClass()
    {
        return embeddedClass;
    }

    public void setEmbeddedClass(EmbeddedClass embeddedClass)
    {
        this.embeddedClass = embeddedClass;
    }

    public LANGUAGES getLanguages()
    {
        return languages;
    }

    public void setLanguages(LANGUAGES languages)
    {
        this.languages = languages;
    }

    public ThreadPoolManagerEventHandler getThreadPoolManager()
    {
        return threadPoolManager;
    }

    public void setThreadPoolManager(ThreadPoolManagerEventHandler threadPoolManager)
    {
        this.threadPoolManager = threadPoolManager;
    };

}
