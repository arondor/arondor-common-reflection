package com.arondor.common.reflection.gwt.client.presenter;

import com.arondor.common.reflection.bean.config.MapConfigurationBean;
import com.arondor.common.reflection.gwt.client.presenter.SimpleObjectConfigurationMapPresenter.MyPrimitiveConfiguration;

import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

public class TestMyPrimitiveConfiguration
{
    private MyPrimitiveConfiguration primitive1, primitive2;

    @Test
    public void testEqualsNull()
    {
        primitive1 = null;
        primitive2 = null;
        Assert.assertTrue(Objects.equals(primitive1, primitive2));
        primitive1 = new MyPrimitiveConfiguration(null);
        Assert.assertFalse(Objects.equals(primitive1, primitive2));
    }

    @Test
    public void testSameObjectInfo()
    {
        primitive1 = new MyPrimitiveConfiguration(null);
        primitive2 = new MyPrimitiveConfiguration(null);
        Assert.assertTrue(Objects.equals(primitive1, primitive2));

        primitive1.setValue("test");
        primitive2.setValue("test");
        Assert.assertTrue(Objects.equals(primitive1, primitive2));
    }

    @Test
    public void testDiffClass()
    {
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(primitive1, other));
    }

    @Test
    public void testDiffValues()
    {
        primitive1 = new MyPrimitiveConfiguration("test");
        primitive2 = new MyPrimitiveConfiguration("no");
        Assert.assertFalse(Objects.equals(primitive1, primitive2));
    }

    @Test
    public void testSameObject()
    {
        primitive2 = primitive1;
        Assert.assertTrue(Objects.equals(primitive1, primitive2));
    }
}
