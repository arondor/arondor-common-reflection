package com.arondor.common.reflection.gwt.client.nview;

import com.arondor.common.reflection.gwt.client.CssBundle;
import com.arondor.common.reflection.gwt.client.presenter.fields.MapTreeNodePresenter.MapPairDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.MapTreeNodePresenter.MapRootDisplay;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;

import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.Position;
import gwt.material.design.client.ui.MaterialCard;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialLink;

public class NMapNodeView extends NNodeView implements MapRootDisplay
{
    private final FlowPanel header = new FlowPanel();

    protected final MaterialCard mappingTable = new MaterialCard();

    private final MaterialLabel label = new MaterialLabel();

    private final MaterialLink newPairBtn = new MaterialLink();

    private final MaterialLabel errorLabel = new MaterialLabel();

    public NMapNodeView()
    {
        getElement().addClassName(CssBundle.INSTANCE.css().mappingField());

        header.getElement().setAttribute("style", "display:flex;position:relative;");

        label.setStyle(
                "color:#9E9E9E;font-size:14px;font-family: Arial Unicode MS, Arial, sans-serif;padding-right:30px;");
        newPairBtn.setIconType(IconType.ADD);

        mappingTable.getElement().addClassName(CssBundle.INSTANCE.css().mappingTable());
        mappingTable.getElement().addClassName("container");

        header.add(label);
        header.add(newPairBtn);

        errorLabel.setStyle(
                "color:#F44336;font-size:12px;font-family: Roboto, sans-serif;padding-left:14px;padding-top:10px");
        errorLabel.setText(getEmptyMandatoryMessage());

        attachElements();
    }

    private void attachElements()
    {
        add(header);
        add(mappingTable);
    }

    @Override
    public void clear()
    {
        super.clear();
        mappingTable.clear();
        attachElements();
    }

    @Override
    public void setNodeDescription(String description)
    {
        label.setText(description);
    }

    @Override
    public HasClickHandlers addElementClickHandler()
    {
        return newPairBtn;
    }

    @Override
    public MapPairDisplay createPair(String keyClass, String valueClass)
    {
        label.getElement().removeClassName(CssBundle.INSTANCE.css().mappingFieldError());
        remove(errorLabel);
        if (getWidgetIndex(mappingTable) == -1)
            add(mappingTable);

        if (keyClass.equals(String.class.getName()) && valueClass.equals(String.class.getName()))
        {
            MapPairDisplay newPair = new NMapPairViewString(keyClass, valueClass);
            mappingTable.add(newPair.asWidget());
            return newPair;
        }
        else if (keyClass.equals(String.class.getName()))
        {
            MapPairDisplay newPair = new NClassNodeViewWithKey(keyClass, valueClass, false);
            mappingTable.add(newPair.asWidget());
            return newPair;
        }
        throw new RuntimeException("Not supported : pair key=" + keyClass + ", value=" + valueClass);
    }

    @Override
    public void highlightUnfilledMandatories()
    {
        if (mappingTable.getChildrenList().isEmpty() && mappingTable.getParent().getParent().getElement()
                .hasClassName(CssBundle.INSTANCE.css().classMandatoryChildren()))
        {
            label.getElement().addClassName(CssBundle.INSTANCE.css().mappingFieldError());
            remove(mappingTable);
            add(errorLabel);
        }

    }

    protected void buildNewPairBntForSharedObject()
    {
        newPairBtn.setTooltip("Add a new Shared object");
        newPairBtn.setTooltipPosition(Position.TOP);
        newPairBtn.getElement().addClassName(CssBundle.INSTANCE.css().newPairBtnSharedObject());
    }

    protected void buildNewPairBntForCatalog()
    {
        newPairBtn.setTooltip("Add");
        newPairBtn.getElement().addClassName(CssBundle.INSTANCE.css().newPairBtn());
    }
}
