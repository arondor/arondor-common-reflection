package com.arondor.common.reflection.gwt.client.nview;

import com.arondor.common.reflection.gwt.client.CssBundle;
import com.arondor.common.reflection.gwt.client.presenter.TreeNodePresenter.Display;
import com.arondor.common.reflection.gwt.client.presenter.fields.MapTreeNodePresenter.MapPairDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.PrimitiveTreeNodePresenter.PrimitiveDisplay;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.RootPanel;

import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialDialog;
import gwt.material.design.client.ui.MaterialDialogFooter;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialTitle;

public class NClassNodeViewWithKey extends NClassNodeView implements MapPairDisplay
{
    private final MaterialTextBox keyTextBox = new MaterialTextBox();

    private final MaterialLink deleteRowBtn = new MaterialLink();

    private final MaterialDialog deleteDialog = new MaterialDialog();

    private final MaterialTitle title = new MaterialTitle();

    private final MaterialDialogFooter dialogFooter = new MaterialDialogFooter();

    private final MaterialButton btnConfirmDelete = new MaterialButton(), btnCancelDelete = new MaterialButton();

    private boolean fromSharedObjectPlace;

    public NClassNodeViewWithKey(String keyClass, String valueClass, boolean fromSharedObject)
    {
        fromSharedObjectPlace = fromSharedObject;

        keyTextBox.setClass("outlined col-4 pl-0");
        keyTextBox.getElement().getElementsByTagName("input").getItem(0).addClassName("mb-0 bg-white");
        keyTextBox.setLabel("Key");
        keyTextBox.setPlaceholder("M_sharedObjectName");

        getOptionsArea().getElement().setAttribute("style", "width:100%;padding-left:10px;");

        getElement().addClassName("p-2 m-0");

        deleteRowBtn.getElement().addClassName(CssBundle.INSTANCE.css().deleteRowBtn());
        deleteRowBtn.setIconType(IconType.DELETE);
        deleteRowBtn.setTooltip("Delete");

        if (fromSharedObjectPlace)
        {
            getElement().setAttribute("style",
                    "background-color:#F5F5F5; margin-bottom : 20px!important; border-radius:5px;");
            deleteRowBtn.getElement().addClassName(CssBundle.INSTANCE.css().deleteSharedObject());
        }
        else
        {
            deleteRowBtn.getElement().addClassName(CssBundle.INSTANCE.css().deletePropertyMap());
        }

        deleteRowBtn.addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                buildDeleteDialog();
                RootPanel.get().add(deleteDialog);
                deleteDialog.open();
            }
        });

        getSelectGroup().getElement().addClassName("col-6 pr-0 pl-0 mb-0");
        getSelectGroup().remove(super.implementingClassView.getSharedObjectCreatePanel());
        getSelectGroup().remove(super.implementingClassView.getSharedObjectForwardPanel());

        bind();

        setNodeDescription("Selected class");
        addClassNameOnSelectedClassContainer(CssBundle.INSTANCE.css().containerSelectedClass());
    }

    public MaterialTextBox getKeyTextBox()
    {
        return keyTextBox;
    }

    private void buildDeleteDialog()
    {
        deleteDialog.setDismissible(true);
        deleteDialog.getElement().addClassName("popupDialog");

        deleteDialog.addOpenHandler(new OpenHandler<MaterialDialog>()
        {
            @Override
            public void onOpen(OpenEvent<MaterialDialog> event)
            {
                btnConfirmDelete.setFocus(true);
            }
        });

        title.getElement().addClassName(CssBundle.INSTANCE.css().titleDialog());

        if (fromSharedObjectPlace)
            title.setTitle("Delete Shared object " + keyTextBox.getText() + " ?");
        else
            title.setTitle("Delete property map " + keyTextBox.getText() + " ?");

        btnCancelDelete.getElement().addClassName(CssBundle.INSTANCE.css().dialogBtn());
        btnCancelDelete.getElement().addClassName(CssBundle.INSTANCE.css().cancelDialogBtn());
        btnCancelDelete.setText("Keep");
        btnCancelDelete.setIconType(IconType.CANCEL);
        btnCancelDelete.addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                deleteDialog.close();
            }
        });

        btnConfirmDelete.getElement().addClassName(CssBundle.INSTANCE.css().dialogBtn());
        btnConfirmDelete.getElement().addClassName(CssBundle.INSTANCE.css().confirmDialogBtn());
        btnConfirmDelete.setText("Confirm");
        btnConfirmDelete.setIconType(IconType.DONE);
        btnConfirmDelete.addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                getElement().removeFromParent();
                deleteDialog.close();
                RootPanel.get().remove(deleteDialog);
            }
        });

        dialogFooter.add(btnCancelDelete);
        dialogFooter.add(btnConfirmDelete);

        deleteDialog.add(title);
        deleteDialog.add(dialogFooter);
    }

    @Override
    protected void attachChildren()
    {
        if (keyTextBox != null)
        {
            add(keyTextBox);
        }
        if (deleteRowBtn != null)
        {
            // add on value change
            add(deleteRowBtn);
        }
        super.attachChildren();
    }

    @Override
    public HasClickHandlers removePairClickHandler()
    {
        return btnConfirmDelete;
    }

    @Override
    public PrimitiveDisplay getKeyDisplay()
    {
        /*
         * Force adding the Key TextBox if is has not been added at bind() time
         */
        if (keyTextBox.getParent() == null)
        {
            insert(keyTextBox, 0);
        }
        return new PrimitiveMaterialDisplay(keyTextBox);
    }

    @Override
    public Display getValueDisplay()
    {
        return this;
    }
}
