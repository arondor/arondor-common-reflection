package com.arondor.common.reflection.gwt.client.nview;

import java.util.List;

import com.arondor.common.reflection.gwt.client.CssBundle;
import com.arondor.common.reflection.gwt.client.api.ObjectConfigurationMapPresenter.MapPairDisplayWithScope;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.client.constants.Color;

public class NScopeClassView extends NClassNodeViewWithKey implements MapPairDisplayWithScope
{
    private MaterialComboBox<String> scopeBox = new MaterialComboBox<String>();

    public NScopeClassView(String keyClass, String valueClass)
    {
        super(keyClass, valueClass, true);
        // scopeBox.getElement().getStyle().setWidth(120f, Unit.PX);
        scopeBox.getElement().setClassName("outlined col-1 pl-0");
        scopeBox.getElement().addClassName(CssBundle.INSTANCE.css().scopeSelector());
        scopeBox.setLabel("Scope");
        scopeBox.setHideSearch(true);

        getKeyTextBox().addKeyUpHandler(new KeyUpHandler()
        {
            @Override
            public void onKeyUp(KeyUpEvent event)
            {
                updateSharedObjectName(getKeyTextBox(), scopeBox);
            }
        });

        scopeBox.addValueChangeHandler(new ValueChangeHandler<List<String>>()
        {

            @Override
            public void onValueChange(ValueChangeEvent<List<String>> event)
            {
                updateSharedObjectName(getKeyTextBox(), scopeBox);
            }
        });
    }

    @Override
    protected void attachChildren()
    {
        if (scopeBox != null)
            add(scopeBox);
        super.attachChildren();
    }

    @Override
    public void setAvailableScopes(List<String> scopes)
    {
        for (String scope : scopes)
            scopeBox.addItem(scope);
    }

    @Override
    public void setScope(String scope)
    {
        scopeBox.setSelectedIndex(scopeBox.getValueIndex(scope));
    }

    @Override
    public String getScope()
    {
        if (scopeBox.getSelectedValue().isEmpty())
            return null;
        return scopeBox.getSelectedValue().get(0);
    }
}