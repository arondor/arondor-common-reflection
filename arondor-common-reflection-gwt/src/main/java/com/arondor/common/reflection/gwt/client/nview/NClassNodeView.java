package com.arondor.common.reflection.gwt.client.nview;

import com.arondor.common.reflection.gwt.client.CssBundle;
import com.arondor.common.reflection.gwt.client.nview.prim.NBooleanView;
import com.arondor.common.reflection.gwt.client.nview.prim.NIntView;
import com.arondor.common.reflection.gwt.client.nview.prim.NPasswordView;
import com.arondor.common.reflection.gwt.client.nview.prim.NScriptView;
import com.arondor.common.reflection.gwt.client.nview.prim.NStringListView;
import com.arondor.common.reflection.gwt.client.nview.prim.NStringView;
import com.arondor.common.reflection.gwt.client.presenter.ClassTreeNodePresenter;
import com.arondor.common.reflection.gwt.client.presenter.ImplementingClassPresenter.ImplementingClassDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.EnumTreeNodePresenter.EnumDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.ListTreeNodePresenter.ListRootDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.MapTreeNodePresenter.MapRootDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.PrimitiveTreeNodePresenter.PrimitiveDisplay;
import com.arondor.common.reflection.gwt.client.presenter.fields.StringListTreeNodePresenter.StringListDisplay;
import com.arondor.common.reflection.gwt.client.view.ImplementingClassView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;

import java.util.List;
import java.util.function.Consumer;

import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialDialog;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialTitle;

public class NClassNodeView extends NNodeView implements ClassTreeNodePresenter.ClassDisplay
{
    protected final ImplementingClassDisplay implementingClassView = new ImplementingClassView();

    private final FlowPanel selectGroup = new FlowPanel();

    private final FlowPanel classGroup = new FlowPanel();

    private final FlowPanel contentGroup = new FlowPanel();

    private final FlowPanel optionsArea = new FlowPanel();

    private final FlowPanel mandatoryChildren = new FlowPanel();

    private final FlowPanel optionalChildren = new FlowPanel();

    private final FlowPanel advancedSettings = new FlowPanel();

    private MaterialLabel classJar = new MaterialLabel();

    private final MaterialDialog convertTaskDialog = new MaterialDialog();

    private final MaterialButton btnCancelConversion = new MaterialButton(), btnConvertTask = new MaterialButton();

    private final MaterialTitle title = new MaterialTitle();

    private final MaterialComboBox<String> scopeList = new MaterialComboBox<String>();

    private final MaterialTextBox keyNameTextBox = new MaterialTextBox();

    private static final String ALLOWED_FOR_NAME = "^[a-zA-Z0-9-_]+$";

    private List<String> availableScopes;

    private boolean hasChildren;

    public NClassNodeView()
    {
        getElement().addClassName(CssBundle.INSTANCE.css().classNode());

        selectGroup.getElement().addClassName("input-group d-flex flex-row-reverse");
        selectGroup.add(implementingClassView.getSharedObjectCreatePanel());
        selectGroup.add(implementingClassView.getSharedObjectForwardPanel());
        classJar.getElement().addClassName(CssBundle.INSTANCE.css().jarClass());
        classGroup.setVisible(false);
        classGroup.add(classJar);

        getResetFieldBtn().getElement().addClassName("input-group-append");
        getResetFieldBtn().getElement().addClassName(CssBundle.INSTANCE.css().resetBtn());
        getResetFieldBtn().getElement().setInnerHTML("<i></i>");

        contentGroup.getElement().addClassName("col-12");

        mandatoryChildren.getElement().addClassName(CssBundle.INSTANCE.css().classMandatoryChildren());
        optionalChildren.getElement().addClassName(CssBundle.INSTANCE.css().classOptionalChildren());

        advancedSettings.getElement().addClassName(CssBundle.INSTANCE.css().advancedSettingsBtn());

        String rnd = String.valueOf(Math.random()).substring(2);
        advancedSettings.getElement()
                .setInnerHTML("<a data-toggle=\"collapse\" aria-expanded=\"false\" href=\"#advancedSettings" + rnd
                        + "\" class=\"collapsed\"></a>");

        advancedSettings.getElement().addClassName(CssBundle.INSTANCE.css().hideAdvancedSettings());

        optionalChildren.getElement().setId("advancedSettings" + rnd);
        optionalChildren.getElement().addClassName("collapse");

        bind();
        attachChildren();
    }

    public List<String> getAvailableScopes()
    {
        return availableScopes;
    }

    public void setAvailableScopes(List<String> availableScopes)
    {
        this.availableScopes = availableScopes;
    }

    private void buildConvertTaskDialog()
    {
        convertTaskDialog.getElement().addClassName(CssBundle.INSTANCE.css().convertTaskDialog());
        convertTaskDialog.getElement().addClassName("popupDialog");

        title.setTitle("Convert task configuration to shared object ? ");
        title.getElement().addClassName(CssBundle.INSTANCE.css().titleDialog());

        keyNameTextBox.setClass("outlined");
        keyNameTextBox.setMargin(10);
        keyNameTextBox.setLabel("Name");
        keyNameTextBox.setPlaceholder("M_sharedObjectName");

        for (String scope : availableScopes)
            scopeList.addItem(scope);

        scopeList.setPlaceholder("Scope");
        scopeList.setHideSearch(true);
        scopeList.getElement().addClassName(CssBundle.INSTANCE.css().scopeSelector());

        btnCancelConversion.getElement().addClassName("dismissPopup");

        // prevent bootstrap overstyle
        btnCancelConversion.getElement().removeClassName("btn");
        btnCancelConversion.setIconType(IconType.CANCEL);

        btnConvertTask.getElement().addClassName(CssBundle.INSTANCE.css().doConversionBtn());
        btnConvertTask.setText("Convert to shared object");
        btnConvertTask.setEnabled(false);

        convertTaskDialog.add(btnCancelConversion);
        convertTaskDialog.add(title);
        convertTaskDialog.add(keyNameTextBox);
        convertTaskDialog.add(scopeList);
        convertTaskDialog.add(btnConvertTask);
    }

    protected FlowPanel getSelectGroup()
    {
        return selectGroup;
    }

    protected void bind()
    {
        convertTaskDialog.addOpenHandler(new OpenHandler<MaterialDialog>()
        {
            @Override
            public void onOpen(OpenEvent<MaterialDialog> event)
            {
                keyNameTextBox.setFocus(true);
            }
        });

        keyNameTextBox.addKeyUpHandler(new KeyUpHandler()
        {
            @Override
            public void onKeyUp(KeyUpEvent event)
            {
                updateSharedObjectName(keyNameTextBox, scopeList);
            }

        });

        scopeList.addValueChangeHandler(new ValueChangeHandler<List<String>>()
        {

            @Override
            public void onValueChange(ValueChangeEvent<List<String>> event)
            {
                updateSharedObjectName(keyNameTextBox, scopeList);
            }
        });

        implementingClassView.getSharedObjectCreatePanel().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                keyNameTextBox.clear();
                RootPanel.get().add(convertTaskDialog);
                buildConvertTaskDialog();
                convertTaskDialog.open();
            }
        });

        btnCancelConversion.addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                convertTaskDialog.close();
                RootPanel.get().remove(convertTaskDialog);
                keyNameTextBox.clear();
            }
        });

        getResetFieldBtn().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                implementingClassView.reset();
                setActive(false);
                clear();
            }
        });
    }

    protected String applySharedObjectPrefix(String key, String scope)
    {
        if (key.startsWith("M_") || key.startsWith("G_") || key.startsWith("C_"))
            key = key.substring(2);
        return scope.substring(0, 1) + "_" + key;
    }

    protected void updateSharedObjectName(MaterialTextBox textBox, MaterialComboBox<String> scopeBox)
    {
        String newName = textBox.getText();

        btnConvertTask.setEnabled(!newName.substring(2).isEmpty() && isNameAuthorized(newName));
        if (!newName.isEmpty())
        {
            newName = applySharedObjectPrefix(newName, scopeBox.getSelectedValue().get(0));
            textBox.setValue(newName, true);
        }
    }

    protected void attachChildren()
    {
        selectGroup.add(implementingClassView);
        selectGroup.add(getResetFieldBtn());
        selectGroup.add(classGroup);
        add(selectGroup);

        optionsArea.add(advancedSettings);
        optionsArea.add(optionalChildren);
        contentGroup.add(mandatoryChildren);
        contentGroup.add(optionsArea);
        add(contentGroup);
    }

    protected FlowPanel getOptionsArea()
    {
        return optionsArea;
    }

    private void addChildView(boolean isMandatory, NNodeView childView)
    {
        if (isMandatory)
        {
            mandatoryChildren.add(childView);
        }
        else
        {
            advancedSettings.getElement().removeClassName(CssBundle.INSTANCE.css().hideAdvancedSettings());
            optionalChildren.add(childView);
        }
    }

    @Override
    public ImplementingClassDisplay getImplementingClassDisplay()
    {
        return implementingClassView;
    }

    @Override
    public ClassTreeNodePresenter.ClassDisplay createClassChild(boolean isMandatory)
    {
        NClassNodeView childView = new NClassNodeView();
        childView.enableReset(!isMandatory);
        childView.setAvailableScopes(getAvailableScopes());
        addChildView(isMandatory, childView);

        return childView;
    }

    @Override
    public PrimitiveDisplay createPrimitiveChild(String fieldClassName, boolean isMandatory)
    {
        NNodeView view;
        if (fieldClassName.equals("boolean"))
        {
            view = new NBooleanView();
        }
        else if (fieldClassName.equals("int"))
        {
            view = new NIntView();
        }
        else if (fieldClassName.equals("java.lang.String"))
        {
            view = new NStringView();
        }
        // else if (fieldClassName.equals("java.util.List"))
        // {
        // view = new NListView();
        // }
        // TODO long
        // TODO java.util.Map
        // TODO java.util.List
        // TODO java.lang.Class
        // TODO java.io.InputStream
        // TODO java.math.BigInteger

        // TODO com.arondor.fast2p8.model.manager.Manager
        // TODO com.arondor.fast2p8.alfresco.PropertyHelper
        // TODO com.arondor.fast2p8.alfresco.AlfrescoConnection
        // TODO com.arondor.fast2p8.model.namingscheme.NamingScheme
        // TODO com.arondor.fast2p8.alfresco.AlfrescoCMISConnectionProvider
        // TODO com.arondor.fast2p8.model.namingscheme.PunnetPatternResolver
        // TODO org.apache.chemistry.opencmis.client.api.Session
        else
        {
            view = new NStringView();
        }

        addChildView(isMandatory, view);
        return (PrimitiveDisplay) view;
    }

    @Override
    public PrimitiveDisplay createPasswordChild(String fieldClassName, boolean isMandatory)
    {
        NNodeView view = new NPasswordView();
        addChildView(isMandatory, view);
        return (PrimitiveDisplay) view;
    }

    @Override
    public PrimitiveDisplay createScriptChild(String scriptType, boolean isMandatory)
    {
        NScriptView view = new NScriptView();
        addChildView(isMandatory, view);
        return view;
    }

    @Override
    public EnumDisplay createEnumListChild(boolean isMandatory)
    {
        NEnumView view = new NEnumView();
        addChildView(isMandatory, view);
        return view;
    }

    @Override
    public StringListDisplay createStringListChild(boolean isMandatory)
    {
        NStringListView view = new NStringListView();
        addChildView(isMandatory, view);
        return view;
    }

    @Override
    public MapRootDisplay createMapChild(boolean isMandatory)
    {
        NMapNodeView mapView = new NMapNodeView();
        mapView.buildNewPairBntForCatalog();
        addChildView(isMandatory, mapView);
        return mapView;
    }

    @Override
    public ListRootDisplay createListChild(boolean isMandatory)
    {
        NListView listView = new NListView();
        addChildView(isMandatory, listView);
        return listView;
    }

    @Override
    public void clear()
    {
        super.clear();
        setJarClass(null);
        setdisplayForChildren(false);
        mandatoryChildren.clear();
        optionalChildren.clear();

        optionalChildren.getElement().removeClassName("show");
        advancedSettings.getElement().addClassName(CssBundle.INSTANCE.css().hideAdvancedSettings());
        attachChildren();
    }

    @Override
    public void setNodeDescription(String description)
    {
        implementingClassView.setNodeDescription(description);
    }

    public void addClassNameOnSelectedClassContainer(String newClassName)
    {
        implementingClassView.addClassNameOnSelectedClassContainer(newClassName);
    }

    /**
     * Add the click handler on share button
     */
    @Override
    public void onShare(Consumer<String> onShare)
    {
        btnConvertTask.addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                onShare.accept(scopeList.getSelectedValue().get(0));
            }
        });

        keyNameTextBox.addKeyDownHandler(new KeyDownHandler()
        {

            @Override
            public void onKeyDown(KeyDownEvent event)
            {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER && isNameAuthorized(keyNameTextBox.getText()))
                {
                    onShare.accept(scopeList.getSelectedValue().get(0));
                }

            }
        });
    }

    private boolean isNameAuthorized(String name)
    {
        if (!RegExp.compile(ALLOWED_FOR_NAME).test(name))
        {
            keyNameTextBox.setErrorText("Allowed : a-z, A-Z, 0-9, -, _");
            return false;
        }
        else
        {
            keyNameTextBox.clearErrorText();
            return true;
        }
    }

    /**
     * Add the click handler
     */
    @Override
    public HandlerRegistration forwardToSharedObject(ClickHandler handler)
    {
        return implementingClassView.getSharedObjectForwardPanel().addClickHandler(handler);
    }

    @Override
    public HandlerRegistration onReset(ClickHandler handler)
    {
        return getResetFieldBtn().addClickHandler(handler);
    }

    @Override
    public void setActive(boolean active)
    {
        super.setActive(active);
        if (!active)
        {
            advancedSettings.getElement().addClassName(CssBundle.INSTANCE.css().hideAdvancedSettings());
        }
        advancedSettings.getElement().getElementsByTagName("a").getItem(0).setClassName("collapsed");
    }

    @Override
    public void closeDialog()
    {
        convertTaskDialog.close();
    }

    @Override
    public String getKeyName()
    {
        return keyNameTextBox.getText();
    }

    @Override
    public void clearKeyName()
    {
        keyNameTextBox.clear();
        keyNameTextBox.setFocus(true);
    }

    @Override
    public void setJarClass(String jarPath)
    {
        String jarName = null;
        if (jarPath != null)
        {
            jarName = jarPath.substring(jarPath.lastIndexOf("/") + 1);
            classGroup.setVisible(true);

        }
        else
        {
            classGroup.setVisible(false);

        }
        classJar.setValue(jarName);
    }

    @Override
    public void setdisplayForChildren(boolean hasChildren)
    {
        if (hasChildren)
            classJar.getElement().addClassName(CssBundle.INSTANCE.css().hasChildren());
        else
            classJar.getElement().removeClassName(CssBundle.INSTANCE.css().hasChildren());

    }

}
