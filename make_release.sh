versionNumber=$1
git checkout -b release/$versionNumber
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$versionNumber 
git commit -a -m "Release $versionNumber"
git push -u origin release/$versionNumber
git tag release/$versionNumber
git push --tags

