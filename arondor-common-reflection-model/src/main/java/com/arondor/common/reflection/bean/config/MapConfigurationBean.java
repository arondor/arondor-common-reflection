/*
 *  Copyright 2013, Arondor
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.arondor.common.reflection.bean.config;

import com.arondor.common.reflection.model.config.ElementConfiguration;
import com.arondor.common.reflection.model.config.MapConfiguration;

import java.util.Map;
import java.util.Objects;

public class MapConfigurationBean extends ElementConfigurationBean implements MapConfiguration
{
    /**
     * 
     */
    private static final long serialVersionUID = 2968136864852828441L;

    @Override
    public ElementConfigurationType getFieldConfigurationType()
    {
        return ElementConfigurationType.Map;
    }

    public MapConfigurationBean()
    {

    }

    private Map<ElementConfiguration, ElementConfiguration> mapConfiguration;

    @Override
    public Map<ElementConfiguration, ElementConfiguration> getMapConfiguration()
    {
        return mapConfiguration;
    }

    @Override
    public void setMapConfiguration(Map<ElementConfiguration, ElementConfiguration> mapConfiguration)
    {
        this.mapConfiguration = mapConfiguration;
    }

    @Override
    public String toString()
    {
        return "MapConfigurationBean[" + mapConfiguration.entrySet() + "]";
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null || (getClass() != obj.getClass()))
            return false;

        MapConfigurationBean other = (MapConfigurationBean) obj;
        return compareMapElementsConfiguration(mapConfiguration, other.mapConfiguration);
    }

    private boolean compareMapElementsConfiguration(Map<ElementConfiguration, ElementConfiguration> map,
            Map<ElementConfiguration, ElementConfiguration> otherMap)
    {
        if ((map == null && otherMap == null))
            return true;
        else if ((map == null && otherMap != null) || (map != null && otherMap == null))
            return false;
        else
        {
            if ((map.size() != otherMap.size()))
                return false;
            else if (map.isEmpty() && otherMap.isEmpty())
                return true;
        }

        for (ElementConfiguration key : map.keySet())
        {
            SearchSameKeys search = doMapsShareSameKey(key, otherMap);
            if (search == null || !search.isSucces())
                return false;

            ElementConfiguration searchedKey = search.getKeySearched();
            if (searchedKey == null)
                return false;

            if (!Objects.equals(map.get(key), otherMap.get(searchedKey)))
                return false;
        }
        return true;
    }

    private SearchSameKeys doMapsShareSameKey(ElementConfiguration key,
            Map<ElementConfiguration, ElementConfiguration> otherMap)
    {
        SearchSameKeys search = new SearchSameKeys();
        for (ElementConfiguration keySearched : otherMap.keySet())
        {
            if (Objects.equals(key, keySearched))
            {
                search.setKeySearched(keySearched);
                search.setSucces(true);
                return search;
            }
        }
        return search;
    }

    private class SearchSameKeys
    {
        private boolean succes = false;

        private ElementConfiguration keySearched = null;

        public boolean isSucces()
        {
            return succes;
        }

        public void setSucces(boolean succes)
        {
            this.succes = succes;
        }

        public ElementConfiguration getKeySearched()
        {
            return keySearched;
        }

        public void setKeySearched(ElementConfiguration keySearched)
        {
            this.keySearched = keySearched;
        }
    }

}
