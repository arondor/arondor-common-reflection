package com.arondor.common.reflection.bean.config;

import com.arondor.common.reflection.model.config.ElementConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

public class TestEqualsObjectConfigurationBean
{
    private ObjectConfigurationBean objectBean1, objectBean2;

    @Test
    public void testOneNotSameClass()
    {
        objectBean1 = createObjectConfigurationBean();
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(objectBean1, other));
    }

    @Test
    public void testSameObjects()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean2 = objectBean1;
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testSameObjectsNull()
    {
        objectBean1 = null;
        objectBean2 = null;
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1 = createObjectConfigurationBean();
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean1 = null;
        objectBean2 = createObjectConfigurationBean();
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));

    }

    @Test
    public void testSameObjectsInfo()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testAttributesCauseVersioning()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean2.setClassName("toto");
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setClassName("test");
        objectBean2.setObjectName("toto");
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setObjectName("test");
        objectBean2.setReferenceName("toto");
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setReferenceName("test");
        objectBean2.setSingleton(false);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setSingleton(true);
        objectBean2.setFullyConfigured(false);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testListsConstructorArguments()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setConstructorArguments(new ArrayList<>());
        objectBean2.setConstructorArguments(new ArrayList<>());
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1.setConstructorArguments(createListConstructorArguments());
        objectBean2.setConstructorArguments(createListConstructorArguments());
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean2.getConstructorArguments().add(new ListConfigurationBean());
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testListsConstructorArgumentsOneNull()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setConstructorArguments(null);
        objectBean2.setConstructorArguments(null);
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1.setConstructorArguments(createListConstructorArguments());
        objectBean2.setConstructorArguments(null);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setConstructorArguments(createListConstructorArguments());
        objectBean1.setConstructorArguments(null);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testListsConstructorArgumentsTypeDiff()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean1.setConstructorArguments(createListConstructorArguments());
        List<ElementConfiguration> list2 = new ArrayList<>();
        list2.add(new ListConfigurationBean());
        list2.add(new ReferenceConfigurationBean());
        objectBean2 = createObjectConfigurationBean();
        objectBean2.setConstructorArguments(list2);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testMapFieldsNull()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setFields(null);
        objectBean2.setFields(null);
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1.setFields(createFields());
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean2.setFields(createFields());
        objectBean1.setFields(null);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testMapFieldsOneNotSameKey()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setFields(createFields());
        Map<String, ElementConfiguration> map2 = new HashMap<String, ElementConfiguration>();
        map2.put("test", new ListConfigurationBean());
        map2.put("no", new ListConfigurationBean());
        objectBean2.setFields(map2);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testSameMapFields()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setFields(new HashMap<String, ElementConfiguration>());
        objectBean2.setFields(new HashMap<String, ElementConfiguration>());
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1.setFields(createFields());
        objectBean2.setFields(createFields());
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        // SizeDiff
        objectBean2.getFields().put("test3", new ListConfigurationBean());
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testMapFieldsTypeDiff()
    {
        objectBean1 = createObjectConfigurationBean();
        objectBean2 = createObjectConfigurationBean();
        objectBean1.setFields(createFields());
        Map<String, ElementConfiguration> map2 = new HashMap<String, ElementConfiguration>();
        map2.put("test", new ListConfigurationBean());
        map2.put("test2", new ReferenceConfigurationBean());
        objectBean2.setFields(map2);
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    private Map<String, ElementConfiguration> createFields()
    {
        Map<String, ElementConfiguration> map = new HashMap<String, ElementConfiguration>();
        map.put("test", new ListConfigurationBean());
        map.put("test2", new ListConfigurationBean());
        return map;
    }

    private List<ElementConfiguration> createListConstructorArguments()
    {
        List<ElementConfiguration> list = new ArrayList<>();
        list.add(new ListConfigurationBean());
        list.add(new ListConfigurationBean());
        return list;
    }

    private ObjectConfigurationBean createObjectConfigurationBean()
    {
        ObjectConfigurationBean obj = new ObjectConfigurationBean();
        obj.setClassName("test");
        obj.setFullyConfigured(true);
        obj.setObjectName("test");
        obj.setReferenceName("test");
        obj.setSingleton(true);
        return obj;
    }
}
