package com.arondor.common.reflection.bean.config;

import com.arondor.common.reflection.model.config.ElementConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

public class TestListConfigurationBean
{
    private ListConfigurationBean listBean1, listBean2;

    @Test
    public void testEqualsNull()
    {
        listBean1 = null;
        listBean2 = null;
        Assert.assertTrue(Objects.equals(listBean1, listBean2));
        listBean1 = new ListConfigurationBean();
        Assert.assertFalse(Objects.equals(listBean1, listBean2));
        listBean2 = new ListConfigurationBean();
        listBean1 = null;
        Assert.assertFalse(Objects.equals(listBean1, listBean2));
    }

    @Test
    public void testEqualsSameObjectInfoSizeDiff()
    {
        listBean1 = new ListConfigurationBean();
        listBean2 = new ListConfigurationBean();
        Assert.assertTrue(Objects.equals(listBean1, listBean2));

        listBean1.setListConfiguration(new ArrayList<>());
        listBean2.setListConfiguration(new ArrayList<>());
        Assert.assertTrue(Objects.equals(listBean1, listBean2));

        listBean1.getListConfiguration().addAll(createListElementsConfiguration());
        listBean2.getListConfiguration().addAll(createListElementsConfiguration());
        Assert.assertTrue(Objects.equals(listBean1, listBean2));

        listBean2.getListConfiguration().add(new PrimitiveConfigurationBean());
        Assert.assertFalse(Objects.equals(listBean1, listBean2));
    }

    @Test
    public void testSameObject()
    {
        listBean1 = new ListConfigurationBean();
        listBean2 = new ListConfigurationBean();
        listBean2 = listBean1;
        Assert.assertTrue(Objects.equals(listBean1, listBean2));
    }

    @Test
    public void testEqualsOtherClass()
    {
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(listBean1, other));
    }

    private List<ElementConfiguration> createListElementsConfiguration()
    {
        List<ElementConfiguration> obj = new ArrayList<>();
        obj.add(new PrimitiveConfigurationBean());
        obj.add(new PrimitiveConfigurationBean());
        return obj;
    }
}
