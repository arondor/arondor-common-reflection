package com.arondor.common.reflection.bean.config;

import java.util.Objects;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestReferenceConfigurationBean
{
    private ReferenceConfigurationBean referenceBean1, referenceBean2;

    @Before
    public void init()
    {
        referenceBean1 = createReferenceConfigurationBean();
    }

    @Test
    public void testEqualsNull()
    {
        referenceBean1 = null;
        referenceBean2 = null;
        Assert.assertTrue(Objects.equals(referenceBean1, referenceBean2));
        referenceBean1 = new ReferenceConfigurationBean();
        Assert.assertFalse(Objects.equals(referenceBean1, referenceBean2));
    }

    @Test
    public void testEqualsSameObjectInfo()
    {
        referenceBean1 = new ReferenceConfigurationBean();
        referenceBean2 = new ReferenceConfigurationBean();
        Assert.assertTrue(Objects.equals(referenceBean1, referenceBean2));

        referenceBean1 = createReferenceConfigurationBean();
        referenceBean2 = createReferenceConfigurationBean();
        Assert.assertTrue(Objects.equals(referenceBean1, referenceBean2));
    }

    @Test
    public void testNotSameInfo()
    {
        referenceBean1 = createReferenceConfigurationBean();
        referenceBean2 = createReferenceConfigurationBean();
        referenceBean2.setReferenceName("no");
        Assert.assertFalse(Objects.equals(referenceBean1, referenceBean2));
    }

    @Test
    public void testSameObject()
    {
        referenceBean2 = referenceBean1;
        Assert.assertTrue(Objects.equals(referenceBean1, referenceBean2));
    }

    @Test
    public void testNotSameClass()
    {
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(referenceBean1, other));
    }

    private ReferenceConfigurationBean createReferenceConfigurationBean()
    {
        ReferenceConfigurationBean obj = new ReferenceConfigurationBean();
        obj.setReferenceName("test");
        return obj;
    }
}
