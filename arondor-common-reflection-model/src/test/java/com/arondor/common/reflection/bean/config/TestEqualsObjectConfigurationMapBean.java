package com.arondor.common.reflection.bean.config;

import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

public class TestEqualsObjectConfigurationMapBean
{

    private ObjectConfigurationMapBean objectBean1, objectBean2;

    @Test
    public void testSameMapsNull()
    {
        objectBean1 = null;
        objectBean2 = null;
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1 = createObjectConfigurationMapBean();
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
        objectBean1 = null;
        objectBean2 = createObjectConfigurationMapBean();
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testSameMapsAndSizeDiff()
    {
        objectBean1 = new ObjectConfigurationMapBean();
        objectBean2 = new ObjectConfigurationMapBean();
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean1 = createObjectConfigurationMapBean();
        objectBean2 = createObjectConfigurationMapBean();
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
        objectBean2.put("test3", new ObjectConfigurationBean());
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testSameObjects()
    {
        objectBean2 = objectBean1;
        Assert.assertTrue(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testNotSameKeyInMaps()
    {
        objectBean1 = createObjectConfigurationMapBean();
        objectBean2 = new ObjectConfigurationMapBean();
        objectBean2.put("test", new ObjectConfigurationBean());
        objectBean2.put("no2", new ObjectConfigurationBean());
        Assert.assertFalse(Objects.equals(objectBean1, objectBean2));
    }

    @Test
    public void testOneNotSameClass()
    {
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(objectBean1, other));
    }

    private ObjectConfigurationMapBean createObjectConfigurationMapBean()
    {
        ObjectConfigurationMapBean map = new ObjectConfigurationMapBean();
        map.put("test", new ObjectConfigurationBean());
        map.put("test2", new ObjectConfigurationBean());
        return map;
    }
}
