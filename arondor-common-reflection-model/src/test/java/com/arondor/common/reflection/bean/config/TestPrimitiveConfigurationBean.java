package com.arondor.common.reflection.bean.config;

import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;

public class TestPrimitiveConfigurationBean
{
    private PrimitiveConfigurationBean primitiveBean1, primitiveBean2;

    @Test
    public void testEqualsNull()
    {
        primitiveBean1 = null;
        primitiveBean2 = null;
        Assert.assertTrue(Objects.equals(primitiveBean1, primitiveBean2));

        primitiveBean1 = new PrimitiveConfigurationBean();
        Assert.assertFalse(Objects.equals(primitiveBean1, primitiveBean2));
    }

    @Test
    public void testEqualsSameObjectInfo()
    {
        primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean2 = new PrimitiveConfigurationBean();
        Assert.assertTrue(Objects.equals(primitiveBean1, primitiveBean2));

        primitiveBean1 = createPrimitiveConfigurationBean("test");
        primitiveBean2 = createPrimitiveConfigurationBean("test");
        Assert.assertTrue(Objects.equals(primitiveBean1, primitiveBean2));
    }

    @Test
    public void testEqualsDiffInfo()
    {
        primitiveBean1 = createPrimitiveConfigurationBean("test");
        primitiveBean2 = createPrimitiveConfigurationBean("no");
        Assert.assertFalse(Objects.equals(primitiveBean1, primitiveBean2));
    }

    @Test
    public void testEqualsSameObject()
    {
        primitiveBean2 = primitiveBean1;
        Assert.assertTrue(Objects.equals(primitiveBean1, primitiveBean2));
    }

    @Test
    public void testNotSameClass()
    {
        MapConfigurationBean other = new MapConfigurationBean();
        Assert.assertFalse(Objects.equals(primitiveBean1, other));
    }

    private PrimitiveConfigurationBean createPrimitiveConfigurationBean(String value)
    {
        PrimitiveConfigurationBean obj = new PrimitiveConfigurationBean();
        obj.setValue(value);
        return obj;
    }
}
