package com.arondor.common.reflection.bean.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import com.arondor.common.reflection.model.config.ElementConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.junit.Test;

public class TestMapConfigurationBean
{
    private MapConfigurationBean mapBean1, mapBean2;

    @Test
    public void test_equals_nullObjects_areEquals()
    {
        mapBean1 = null;
        mapBean2 = null;
        assertTrue(Objects.equals(mapBean1, mapBean2));
        mapBean1 = new MapConfigurationBean();
        assertFalse(Objects.equals(mapBean1, mapBean2));
        mapBean2 = new MapConfigurationBean();
        mapBean1 = null;
        assertFalse(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_objectWithSameInfoAndSize_areEquals()
    {

        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        assertTrue(Objects.equals(mapBean1, mapBean2));

        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        assertTrue(Objects.equals(mapBean1, mapBean2));

        mapBean1.getMapConfiguration().putAll(createMapElementsConfiguration());
        mapBean2.getMapConfiguration().putAll(createMapElementsConfiguration());
        assertTrue(Objects.equals(mapBean1, mapBean2));

        mapBean2.getMapConfiguration().put(new ReferenceConfigurationBean(), new ReferenceConfigurationBean());
        assertFalse(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_sameObject_areEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = mapBean1;
        assertTrue(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_notSameClasses_notEquals()
    {
        ListConfigurationBean other = new ListConfigurationBean();
        assertFalse(Objects.equals(mapBean1, other));
    }

    @Test
    public void test_equals_notSameConfigByValue_notEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());

        PrimitiveConfigurationBean primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean1.setValue("toto");
        PrimitiveConfigurationBean primitiveBean2 = new PrimitiveConfigurationBean();
        primitiveBean2.setValue("totu");
        PrimitiveConfigurationBean primitiveBean3 = new PrimitiveConfigurationBean();
        primitiveBean3.setValue("toti");

        assertNotEquals(primitiveBean1.getValue(), primitiveBean2.getValue());
        assertNotEquals(primitiveBean1.getValue(), primitiveBean3.getValue());
        assertNotEquals(primitiveBean2.getValue(), primitiveBean3.getValue());

        mapBean1.getMapConfiguration().put(primitiveBean1, primitiveBean2);
        mapBean2.getMapConfiguration().put(primitiveBean1, primitiveBean3);

        assertFalse(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_notSameConfigByKey_notEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());

        PrimitiveConfigurationBean primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean1.setValue("toto");
        PrimitiveConfigurationBean primitiveBean2 = new PrimitiveConfigurationBean();
        primitiveBean2.setValue("totu");
        PrimitiveConfigurationBean primitiveBean3 = new PrimitiveConfigurationBean();
        primitiveBean3.setValue("toti");
        PrimitiveConfigurationBean primitiveBean4 = new PrimitiveConfigurationBean();
        primitiveBean4.setValue("tota");

        assertNotEquals(primitiveBean1.getValue(), primitiveBean2.getValue());
        assertNotEquals(primitiveBean1.getValue(), primitiveBean3.getValue());
        assertNotEquals(primitiveBean2.getValue(), primitiveBean3.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean1.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean2.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean3.getValue());

        mapBean1.getMapConfiguration().put(primitiveBean1, primitiveBean2);
        mapBean2.getMapConfiguration().put(primitiveBean3, primitiveBean4);

        assertFalse(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_notSameByValue_notEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());

        String key = "toto";
        PrimitiveConfigurationBean primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean1.setValue(key);
        PrimitiveConfigurationBean primitiveBean2 = new PrimitiveConfigurationBean();
        primitiveBean2.setValue("totu");
        PrimitiveConfigurationBean primitiveBean3 = new PrimitiveConfigurationBean();
        primitiveBean3.setValue(key);
        PrimitiveConfigurationBean primitiveBean4 = new PrimitiveConfigurationBean();
        primitiveBean4.setValue("tota");

        assertNotEquals(primitiveBean1.getValue(), primitiveBean2.getValue());
        assertEquals(primitiveBean1.getValue(), primitiveBean3.getValue());
        assertNotEquals(primitiveBean2.getValue(), primitiveBean3.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean1.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean2.getValue());
        assertNotEquals(primitiveBean4.getValue(), primitiveBean3.getValue());

        mapBean1.getMapConfiguration().put(primitiveBean1, primitiveBean2);
        mapBean2.getMapConfiguration().put(primitiveBean3, primitiveBean4);

        assertFalse(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_sameByKeyAndValue_areEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());

        String key = "toto";
        String value = "tutu";
        PrimitiveConfigurationBean primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean1.setValue(key);
        PrimitiveConfigurationBean primitiveBean2 = new PrimitiveConfigurationBean();
        primitiveBean2.setValue(value);
        PrimitiveConfigurationBean primitiveBean3 = new PrimitiveConfigurationBean();
        primitiveBean3.setValue(key);
        PrimitiveConfigurationBean primitiveBean4 = new PrimitiveConfigurationBean();
        primitiveBean4.setValue(value);

        mapBean1.getMapConfiguration().put(primitiveBean1, primitiveBean2);
        mapBean2.getMapConfiguration().put(primitiveBean3, primitiveBean4);

        assertTrue(Objects.equals(mapBean1, mapBean2));
    }

    @Test
    public void test_equals_sameByObjectKeyAndObjctValue_areEquals()
    {
        mapBean1 = new MapConfigurationBean();
        mapBean2 = new MapConfigurationBean();
        mapBean1.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());
        mapBean2.setMapConfiguration(new HashMap<ElementConfiguration, ElementConfiguration>());

        String key = "toto";
        String value = "tutu";
        PrimitiveConfigurationBean primitiveBean1 = new PrimitiveConfigurationBean();
        primitiveBean1.setValue(key);
        PrimitiveConfigurationBean primitiveBean2 = new PrimitiveConfigurationBean();
        primitiveBean2.setValue(value);
        assertNotEquals(primitiveBean1.getValue(), primitiveBean2.getValue());

        mapBean1.getMapConfiguration().put(primitiveBean1, primitiveBean2);
        mapBean2.getMapConfiguration().put(primitiveBean1, primitiveBean2);

        assertTrue(Objects.equals(mapBean1, mapBean2));
    }

    private Map<ElementConfiguration, ElementConfiguration> createMapElementsConfiguration()
    {
        Map<ElementConfiguration, ElementConfiguration> obj = new HashMap<ElementConfiguration, ElementConfiguration>();
        obj.put(new ReferenceConfigurationBean(), new ReferenceConfigurationBean());
        obj.put(new ReferenceConfigurationBean(), new ReferenceConfigurationBean());
        return obj;
    }
}
