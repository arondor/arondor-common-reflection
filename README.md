# Arondor Common Reflection

Arondor's Reflection library, bring Spring beans to GWT and more

## Get developing on ACR

// Todo

## Publish new release

1. Checkout `master` and `git pull`
1. Run the `make_release.sh`

    ```sh
    sh ./make_release.sh X.Y.Z
    ```
    
    where 'X.Y.Z' is the version you wish to create.

    This script will create a new branch "release"

1. Once pushed to BitBucket, this branch will automatically trigger the Jenkins pipeline for artifactory deployment.